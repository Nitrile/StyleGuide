# C++

## Indentation and Formatting

The indentation format is Allman. e.g.

```cpp
class Object
{
public:
	void exec()
	{
		for (int i = 0; i < 5; ++i)
		{
			std::cout << i << '\n';
		}
	}
};
```

All indentations are done in tabs. All alignments are in spaces. A rule of
thumb is that changing indentation should not disrupt alignment, but any
viewer is free to choose the amount of indentation that is the most
aesthetically pleasing. Line length must not exceed 80 characters under a tab
size of 2 (with some exceptions)

###### Namespaces

Namespaces are not indented. e.g.
```cpp
namespace vct
{

class Parser
{
public:
	Parser();
};

} // namespace vct
```
The closing bracket of a namespace must be followed by a comment indicating the
namespace.
```cpp
namespace dyn
{
...
} // namespace dyn
```
Anonymous namespaces use `<anonymous>`
```cpp
namespace
{
...
} // namespace <anonymous>
```

###### Initialisers

Initialisers follow this convention:
```cpp
Class::Class()
	: field1_(init1_)
	, field2_(init2_)
{}
```
or, with the presence of a parent class
```cpp
Class::Class()
	: ParentClass()
	, field1_(init1_)
	, field2_(init2_)
{}
```
## Naming

* Class names start with a capital letter and are in `CamelCase`.
* Derived class, if suitable, has the format [BaseClass]Derived. e.g. An
  instance of `Particle` could be `ParticleBoson`. Note that this only applies
  if the derived class is closely coupled to the mechanisms of the parent
  class.
* With the exception of types directly coupled to the primitive (`float`,
  `double`, `int32`, etc) types, all types must begin with a Capital Letter.
* Namespaces use `underscore_lower_case`. In reality, an underscore is rarely
  needed.
* Local variables, function arguments, and functions use `lowerCamelCase`.
* Member variables use `lowerCamelCase_`, followed by an underscore. Static
  member variables also follow this convention.
* Constants and macros use `UNDERSCORE_UPPER_CASE`

## Mechanism

Avoid macros.
